/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gREQUEST_STATUS_OK = 200; 
const gREQUEST_STATUS_POST_SUCCESS = 201; 
const gREQUEST_STATUS_DELETE_SUCCESS = 204; 
const gREQUEST_READY_STATUS_FINISH_AND_OK = 4;

const gBASE_URL = "http://203.171.20.210:8080/devcamp-pizza365/orders";

const gORDER_COLS = [
    "stt",
    "orderCode",
    "kichCo",
    "loaiPizza",
    "idLoaiNuocUong",
    "thanhTien",
    "hoTen",
    "soDienThoai",
    "trangThai",
    "action"];


const gORDER_ID_COL = 0;
const gORDER_CODE_COL = 1;
const gORDER_SIZE_COL = 2;
const gORDER_PIZZA_TYPE_COL = 3;
const gORDER_DRINK_TYPE_COL = 4;
const gORDER_PRICE_COL = 5;
const gORDER_FULLNAME_COL = 6;
const gORDER_PHONE_COL = 7;
const gORDER_STATUS_COL = 8;
const gORDER_ACTION_COL = 9;


var vObjectRequest = {
    kichCo: "",
    duongKinh: "",
    suon: "",
    salad: "",
    loaiPizza: "",
    idVourcher: "",
    idLoaiNuocUong: "",
    soLuongNuoc: "",
    hoTen: "",
    thanhTien: "",
    email: "",
    soDienThoai: "",
    diaChi: "",
    loiNhan: ""
};

var gID = null;

var gPizzaSize = {     
    kichCo: "",
    duongKinh: "",
    suon: "",
    salad: "",
    soLuongNuoc: "",
    thanhTien: "",
};

var gPhanTramGiamGia = 0; 

/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
    
    onBtnGetAllOrderClick();
    onClickBtnThemOrder();
    onClickBtnUpdateOrder();
    onClickBtnDelete();
    onClickBtnFilter();
})
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
//ham them order
function onClickBtnThemOrder() {
    $("#btn-create-new").on("click", function () {  
        $("#modal-create").modal("show");
        
        getDataPizzaSizeSelect("select-pizza-size-create");
        
        onPizzaSizeChange();
       
        getDataPizzaTypeSelect("select-pizza-type-create");
        
        onBtnGetDrinkListClick("select-drink-create");
        
        onClickBtnThemOrderConfirm();
    })
}
//load du lieu khi kích cỡ pizza thay đổi
function onPizzaSizeChange() {
    $("#select-pizza-size-create").on("change", function () {
        var vSizePizza = $(this).val();
       
        gPizzaSize = themDuLieuInputKichCo(vSizePizza)
        
        $("#inp-duong-kinh-create").val(gPizzaSize.duongKinh);
        $("#inp-suon-create").val(gPizzaSize.suon);
        $("#inp-salad-create").val(gPizzaSize.salad);
        $("#inp-soluong-nuoc-create").val(gPizzaSize.soLuongNuoc);
        $("#inp-thanh-tien-create").val(gPizzaSize.thanhTien);
    })
}


function onClickBtnThemOrderConfirm() {
    $("#btn-confirm-create").on("click", function () {
        themDuLieuObj(vObjectRequest, "create");
        if (validateObj(vObjectRequest)) {
            console.log(vObjectRequest)
            $("#modal-create").modal("hide");
            alert("Thêm Order thành công!");
            onBtnCreateOrderClick(vObjectRequest)
        }
    })
}
function onClickBtnConfirmUpdateOrder() {
    $("#btn-confirm-edit").on("click", function () {
        onBtnUpdateOrderClick(gID, "confirm");
        $("#modal-edit").modal("hide");
    })
}
function onClickBtnUpdateOrder() {
    $("#table-users tbody").on("click", ".edit-grade", function () {
        onUserUpdateClick(this);
    });
}
function onClickBtnCancelUpdateOrder() {
    $("#btn-cancel-edit").on("click", function () {
        onBtnUpdateOrderClick(gID, "cancel");
        $("#modal-edit").modal("hide");
    })
}
function onClickBtnFilter() {
    $("#btn-filter").on("click", function () {
        onBtnGetAllOrderClick();
    })
}


function onClickBtnDelete() {
    $("#table-users tbody").on("click", ".delete-grade", function () {
        onClickDelete(this);
    });
}
function onClickBtnConfirmDelete() { 
    $("#btn-confirm-delete").on("click", function () {
        // console.log(gID);
        onBtnDeleteOrderClick(gID);
        $("#delete-confirm-modal").modal("hide");
    })
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

function getDataOnDatatable(gJsonObj) {
    // var gUserObj = JSON.parse(gJsonUser);
    $("#table-users").DataTable().destroy();
    $("#table-users").DataTable({
        data: gJsonObj,
        columns: [
            { data: gORDER_COLS[gORDER_ID_COL] },
            { data: gORDER_COLS[gORDER_CODE_COL] },
            { data: gORDER_COLS[gORDER_SIZE_COL] },
            { data: gORDER_COLS[gORDER_PIZZA_TYPE_COL] },
            { data: gORDER_COLS[gORDER_DRINK_TYPE_COL] },
            { data: gORDER_COLS[gORDER_PRICE_COL] },
            { data: gORDER_COLS[gORDER_FULLNAME_COL] },
            { data: gORDER_COLS[gORDER_PHONE_COL] },
            { data: gORDER_COLS[gORDER_STATUS_COL] },
            { data: gORDER_COLS[gORDER_ACTION_COL] },
        ],
        columnDefs: [
            {
                targets: gORDER_ID_COL,
                render: function (data, type, row, meta) {
                    return (meta.row + 1);
                }
            },
            {
                targets: gORDER_ACTION_COL,
                defaultContent: `
                <img class="edit-grade" src="https://cdn0.iconfinder.com/data/icons/glyphpack/45/edit-alt-512.png" style="width: 20px;cursor:pointer;">
                <img class="delete-grade" src="https://cdn4.iconfinder.com/data/icons/complete-common-version-6-4/1024/trash-512.png" style="width: 20px;cursor:pointer;">
              `
            }
        ]
    });
}
function onUserUpdateClick(paramUserDetailBtn) {
    var vUserDetailBtn = $(paramUserDetailBtn);
    var gUserTable = $("#table-users").DataTable();
    var vTableRow = vUserDetailBtn.parents("tr");
    var vDataTableRow = gUserTable.row(vTableRow);
    var vData = vDataTableRow.data();
    gID = vData.id;
    var vOrderCode = vData.orderCode;
    onBtnGetOrderByIdClick(vOrderCode);
    $("#modal-edit").modal("show");
    onClickBtnConfirmUpdateOrder();
    onClickBtnCancelUpdateOrder();
}
function onClickDelete(paramStudent) {
    var vUserDetailBtn = $(paramStudent);
    var gUserTable = $("#table-users").DataTable();
    var vTableRow = vUserDetailBtn.parents("tr");
    var vDataTableRow = gUserTable.row(vTableRow);
    var vData = vDataTableRow.data();
    gID = vData.id;
    $("#delete-confirm-modal").modal("show");
    onClickBtnConfirmDelete();
}
function getDataPizzaSizeSelect(id) {
    var vObj = ["S", "M", "L"];
    var vSelectElm = $("#" + id);
    for (let i = 0; i < vObj.length; i++) {
        $("<option>", {
            value: vObj[i],
            html: vObj[i]
        }).appendTo(vSelectElm);
    }
}
function themDuLieuInputKichCo(param) {
    const gSizePizzaS = {
        kichCo: "S",
        duongKinh: "20",
        suon: "2",
        salad: "200",
        soLuongNuoc: "2",
        thanhTien: "150000",
    }
    const gSizePizzaM = {
        kichCo: "M",
        duongKinh: "25",
        suon: "4",
        salad: "300",
        soLuongNuoc: "3",
        thanhTien: "200000",
    }
    const gSizePizzaL = {
        kichCo: "L",
        duongKinh: "30",
        suon: "8",
        salad: "500",
        soLuongNuoc: "4",
        thanhTien: "250000",
    }

    const gSizeNull = {
        kichCo: "",
        duongKinh: "",
        suon: "",
        salad: "",
        soLuongNuoc: "",
        thanhTien: "",
    }

    if (param == "S") {
        return gSizePizzaS;
    } else if (param == "M") {
        return gSizePizzaM;
    } else if (param == "L") {
        return gSizePizzaL;
    }
    return gSizeNull;
}
function getDataPizzaTypeSelect(id) {
    var vObj = [
        { "id": "Seafood", "name": "Hải sản" },
        { "id": "Hawaii", "name": "Hawaii" },
        { "id": "Bacon", "name": "Thịt hun khói" }
    ]
    var vSelectElm = $("#" + id);
    for (let i = 0; i < vObj.length; i++) {
        $("<option>", {
            value: vObj[i].id,
            html: vObj[i].name
        }).appendTo(vSelectElm);
    }
}
function getDataDrinkSelect(paramObj, id) {
    var vSelectElm = $("#" + id);
    for (let i = 0; i < paramObj.length; i++) {
        $("<option>", {
            value: paramObj[i].maNuocUong,
            html: paramObj[i].tenNuocUong
        }).appendTo(vSelectElm);
    }
}
function themDuLieuObj(paramObj, id) {
    paramObj.kichCo = gPizzaSize.kichCo;
    paramObj.duongKinh = gPizzaSize.duongKinh
    paramObj.suon = gPizzaSize.suon;
    paramObj.salad = gPizzaSize.salad;
    paramObj.loaiPizza = $("#select-pizza-type-" + id).val();
    paramObj.idVourcher = $("#inp-voucher-" + id).val();
    paramObj.idLoaiNuocUong = $("#select-drink-" + id).val();
    paramObj.soLuongNuoc = gPizzaSize.soLuongNuoc
    paramObj.hoTen = $("#inp-name-" + id).val();
    paramObj.thanhTien = gPizzaSize.thanhTien;
    paramObj.email = $("#inp-email-" + id).val();
    paramObj.soDienThoai = $("#inp-phone-" + id).val();
    paramObj.diaChi = $("#inp-address-" + id).val();
    paramObj.loiNhan = $("#txtarea-message-" + id).val();
}
function themDuLieuVaoForm(paramObj) {
    $("#inp-pizza-size-update").val(paramObj.kichCo);
    $("#inp-duong-kinh-update").val(paramObj.duongKinh);
    $("#inp-suon-update").val(paramObj.suon);
    $("#inp-salad-update").val(paramObj.salad);
    $("#inp-soluong-nuoc-update").val(paramObj.soLuongNuoc);
    $("#inp-thanh-tien-update").val(paramObj.thanhTien);
    $("#inp-pizza-type-update").val(paramObj.loaiPizza);
    $("#inp-drink-update").val(paramObj.idLoaiNuocUong);
    $("#inp-name-update").val(paramObj.hoTen);
    $("#inp-email-update").val(paramObj.email);
    $("#inp-phone-update").val(paramObj.soDienThoai);
    $("#inp-address-update").val(paramObj.diaChi);
    $("#inp-voucher-update").val(paramObj.idVourcher);
    $("#txtarea-message-update").val(paramObj.loiNhan);
}
function validateObj(paramObj) {
    if (paramObj.kichCo == 0) {
        alert("Hãy chọn kích cỡ Pizza.");
        return false;
    }
    if (paramObj.loaiPizza == 0) {
        alert("Hãy chọn loại Pizza.");
        return false;
    }
    if (paramObj.idLoaiNuocUong == 0) {
        alert("Hãy chọn loại đồ uống.");
        return false;
    }
    if (paramObj.hoTen == "") {
        alert("Họ tên không được để trống.");
        return false;
    }
    if (!validateEmail(paramObj.email)) {
        alert("Email không đúng định dạng.");
        return false;
    }
    if (paramObj.soDienThoai == "") {
        alert("Số điện thoại không được để trống.");
        return false;
    }
    if (paramObj.diaChi == "") {
        alert("Địa chỉ không được để trống.");
        return false;
    }
    var vCheckVoucher = onBtnCheckVoucherClick(paramObj.idVourcher);
    if (!vCheckVoucher) {
        alert("Voucher không đúng.");
        return false;
    }
    return true;
}
function validateEmail(vEmail) {
    if (vEmail != "") {
        var vRegEmail = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
        return vEmail.match(vRegEmail);
    }
    return true;
}
function filterStudent(vparamObj) {
    var vPizzaTypeFilter = $("#select-pizza-type-filter").val();
    var vStatusFilter = $("#select-status-filter").val();
    if (vPizzaTypeFilter != "0" || vStatusFilter != "0") {
        var vNewObj = vparamObj.filter(function (vObj) {
            if (vPizzaTypeFilter == "0") {
                return vObj.trangThai == vStatusFilter;
            } else if (vStatusFilter == "0") {
                return vObj.loaiPizza == vPizzaTypeFilter;
            } else {
                return (vObj.loaiPizza == vPizzaTypeFilter && vObj.trangThai == vStatusFilter);
            }
        })
        return vNewObj;
    }
    return vparamObj;
}